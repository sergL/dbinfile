#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include "headers/IOUtil.h"

int main() {
    char *path = "D:\\myProjects\\projects\\myDB\\resources\\myDB.txt";
    char *mode = "r+";
    FILE* file = open(path, mode);

    if (file == NULL){
        printf("error");
        return 1;
    }
    int start = 89;
    int finish = 97;

    char * temp = (char*)malloc(finish+start);

    read_position_in_file(file, temp, start, finish);

    printf("%s", temp);

    temp = "test";

    write_position_from_file(file, temp, start,strlen(temp));

    temp = (char*)malloc(finish+start);

    read_position_in_file(file, temp, start, finish);

    printf("%s", temp);

    fclose(file);

    return 0;
}
