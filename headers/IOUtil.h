
#include <stdio.h>

#ifndef MYDB_IOUTIL_H
#define MYDB_IOUTIL_H
FILE * open(const char * path, const char * mode);

char read_position_in_file(FILE* file, char* buff, int start, int finish);

void read(FILE* file, char* buff);

void write_position_from_file(FILE* file, char* buff, int start, int finish);

void write(FILE* file, char* buff);
#endif MYDB_IOUTIL_H
