#include <stdio.h>

FILE* open(const char * path, const char * mode){
    FILE* file;

    file = fopen(path, mode);

    if (file == NULL){
        printf("error");
        return NULL;
    }

    return file;
}

char read_position_in_file(FILE* file, char* buff, int start, int finish){

    fseek(file, start, SEEK_SET);

    fgets(buff, finish-start+1, file);

    return *buff;
}

void write_position_from_file(FILE* file, char* buff, int start, int finish){
    fseek(file, start, SEEK_SET);

    fwrite(buff, finish, 1, file);
}